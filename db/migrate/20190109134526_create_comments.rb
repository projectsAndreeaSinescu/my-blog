class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.string :content
      t.references :user, references: :users
      t.references :article, references: :articles
      t.references :state, references: :states

      t.timestamps
    end
  end
end
