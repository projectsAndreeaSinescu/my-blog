class CreateArticles < ActiveRecord::Migration[5.2]
  def change
    create_table :articles do |t|
      t.string :nume
      t.string :content
      t.string :img_url
      t.references :user, references: :users

      t.timestamps
    end
  end
end
