class Article < ApplicationRecord
	validates :nume, presence: true, length: { minimum: 5, maximum: 20 }
	validates :content, presence: true
	
	has_and_belongs_to_many :categories
	belongs_to :user
	has_many :comments, dependent: :destroy
	has_one_attached :image
end
