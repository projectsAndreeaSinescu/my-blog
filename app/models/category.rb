class Category < ApplicationRecord
	validates :nume, presence: true, length: { minimum: 5 }, uniqueness: true, format: { with: /\A[a-zA-Z]+\z/,
    message: "only allows letters" }

	has_and_belongs_to_many :articles
end
