class Comment < ApplicationRecord
	validates :content, presence: true
	
	belongs_to :state
	belongs_to :user
	belongs_to :article

    scope :published, -> { where(state_id: 1)}
	scope :archived, -> { where(state_id: 2)}
end
