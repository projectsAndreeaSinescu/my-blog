json.extract! user, :id, :email, :created_at, :updated_at
json.url category_url(user, format: :json)
