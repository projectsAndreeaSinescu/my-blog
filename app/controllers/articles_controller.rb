class ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  
  def index
    @articles = Article.all
  end
  
  def show

  end

  def new 
  	@article = Article.new
  end

  def create 
  	@article = Article.new article_params
  	@article.user_id = current_user.id
  	if @article.save
  	  redirect_to articles_path, notice: "Article successfully created."
    else
      render action: :new
    end
  end

  def edit

  end

  def update
  	if @article.update article_params
      redirect_to articles_path, notice: "Article successfully updated."
    else
      render action: :edit
    end
  end

  def destroy
    if @article.destroy
      redirect_to articles_path, notice: "Article successfully deleted."
    else
      redirect_to articles_path, alert: "Cannot delete article."
    end
  end

  private 
  def article_params
  	params.require(:article).permit(:nume, :content, :image, category_ids: [])
  end

  def set_article
  	@article = Article.find params[:id]
  end

  def categories
  	Category.all
  end
  helper_method :categories
end