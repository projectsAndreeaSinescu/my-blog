class CommentsController < ApplicationController
  def create
    @comment = Comment.new comment_params
    @comment.user_id = current_user.id 
    @article = Article.find(params[:article_id])
    @comment.article_id = params[:article_id]
    @comment.state_id = 1
    if @comment.save
    	
    end
    redirect_to article_path(@article)
    
  end

  def destroy
  	@article = Article.find(params[:article_id])
  	@comment = @article.comments.find(params[:id])
  	@comment.destroy
  	redirect_to article_path(@article)
  end

  private 
  def comment_params
  	params.require(:comment).permit(:content)
  end
end