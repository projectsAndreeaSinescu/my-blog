module ApplicationHelper
  def flash_error(name, msg)
    css_classes = ['alert']
    css_classes << case name
    when 'notice'
      'alert-success'
    when 'alert'
      'alert-danger'
    else
      ''
    end

    content_tag(:div, msg, class: css_classes)
  end
end