Rails.application.routes.draw do
  get 'users/index'
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth' }

  resources :users, only: [:index]

  resources :articles do
  	resources :comments, only: [:create, :destroy]
  end

  resources :categories

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'users#index'
end
